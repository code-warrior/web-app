-- This entire database can be constructed by running “source setup.sql” from mysql.
CREATE DATABASE `web_app` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;

USE `web_app`;

CREATE USER 'the_user'@'localhost' IDENTIFIED BY 'f(D2Whiue9d8yD';
GRANT ALL PRIVILEGES ON web_app.* TO 'the_user'@'localhost';

source file.sql;
source user.sql;
