# Web App

v0.4.0

This project is a simple web application that allows users to create usernames and passwords, and to upload/retrieve files from a local server. To stand up this project, you’ll need a local MAMP stack. Facility with The Terminal is assumed, as is the installation of [this special Bash configuration](http://roy.vanegas.org/teaching/files/bash-environment-v1.1.0.tar).

**Note**: In the directions below, `[bash ~]` denotes your terminal, and you’d type the content following it. For example, `[bash ~] ls` means you’d only type the command `ls`.

## Installation
### MySQL Setup

1. Launch The Terminal.
2. Start MySQL. (You will likely be prompted for your computer’s root password.)
```bash
[bash ~] startmysql
```
The Terminal will respond with two lines of content, and its cursor will hang. This isn’t a problem; it’s exactly how The Terminal should behave.
3. Close The Terminal window you just created.
4. Open a new instance of The Terminal.
5. Navigate to the `mysql` folder in this repo.
```bash
[bash ~] cd PATH/TO/WEB-APP/mysql
```
6. From the `mysql` folder, log in to MySQL as the `root` user, prompting MySQL to ask for your MySQL root password, *not* your OS root password:
```bash
[bash ~] mysql -u root -p
```
7. Provide MySQL with the contents of `setup.sql`, which will create a database called `web_app` consisting of tables `file` and `user`.
```mysql
mysql> source setup.sql
```
Alternatively, you could have loaded the contents of `setup.sql` directly from the shell: `[bash ~] mysql -u root -p < setup.sql`.
8. Log out of MySQL to go back to the shell.
```bash
mysql> exit
```
9. Log back in with the credentials used in `setup.sql`. **Note**: The password for which you will be prompted is in files `mysql/setup.sql` and `includes/config.php`.
```bash
[bash ~] mysql -u the_user -p
```
10. List the databases to which you have access.
```mysql
mysql> show databases;
```
You should see `web_app` as one of the databases.
11. Exit MySQL
```mysql
mysql> exit;
```

---

### Project Folder Name and Location
The folder name of this project should be `web-app`. Please rename it if it’s anything other than `web-app`. For example, if you downloaded a ZIP version of this project instead of cloning it via Git.

Move `web-app` into your local server’s document root. In most macOS installations, that may be a folder called `Sites` under your home folder.

---

### File and Folder Permissions
The final task is to ensure that the files and folders inside `web-app` have `755` permissions.

1. Navigate to the `web-app` folder.
```shell
[bash ~] cd ~/Sites/web-app/
```
2. Give every file in `web-app` `755` permission.
```shell
[bash ~] chmod 755 *
```
3. Give the `uploads` folder `777` permission.
```shell
[bash ~] chmod 777 uploads
```

Your web application is now ready. Assuming your web server and your MySQL server are running, open a browser and point it at the `web-app` folder. In some version of macOS, the path might be `localhost/~YOUR_USERNAME/web-app`, where `YOUR_USERNAME` is your Mac username. In others, it might simply be `localhost/web-app`.
