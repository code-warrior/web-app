<?php
/**
 * Initial Index/Landing Page
 *
 * A basic login page.
 *
 * PHP version 5.3.28
 *
 * @category Web_App
 * @package  Web_App
 * @author   Roy A Vanegas <roy@thecodeeducators.com>
 * @license  https://gnu.org/licenses/gpl.html GNU General Public License
 * @link     https://bitbucket.org/code-warrior/web-app/
 */

session_start();

if (isset($_SESSION["valid"])) {
    if (1 == $_SESSION["valid"]) {
        header("Location: home.php");
    }
}

if (!isset($_GET['action'])) {
    $_GET['action'] = "register";
    header("Location: ./index.php?action=". $_GET['action']);
}

require "includes/main.php";

if (isset($_GET['action'])) {
    if ("login" == $_GET['action']) {
        $action_value = "login.php";
        $subheading = $button_value = "Login";
    } else {
        if ("register" == $_GET['action']) {
            $action_value = "register.php";
            $subheading = $button_value = "Register";
        }
    }
}
?>
<!DOCTYPE HTML>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Web Application (v0.3.1)</title>
      <link rel="stylesheet" href="css/normalize.css">
      <link rel="stylesheet" href="css/style.css">
   </head>
   <body>
      <header>
         <h1>Web App</h1>
         <p><?php echo $subheading ?></p>
      </header>
      <p>Would you like to <a href="./index.php?action=register">register</a> or
          <a href="./index.php?action=login">login?</a></p>
      <form action="<?php echo $action_value; ?>" method="post">
         <p><input type="text"
                   name="username"
                   placeholder="username"
                   required
                   autofocus></p>
         <p><input type="password"
                   name="password"
                   placeholder="password" required></p>
         <p><input type="hidden" name="submitted" value="1"></p>
         <p><input type="submit" value="<?php echo $button_value; ?>"></p>
      </form>
   </body>
</html>
