<?php
/**
 * The home page of a registered user
 *
 * If a user’s login credentials have been verified by the logic in
 * “login.php,” then $_SESSION['valid'] is set to true and the user is redirected
 * here. If $_SESSION['valid'] is not true, then the user is redirected to the index
 * page.
 *
 * A valid user can upload files using the form on this page, and see the files
 * she/he has uploaded.
 *
 * PHP version 5.3.28
 *
 * @category Web_App
 * @package  Web_App
 * @author   Roy A Vanegas <roy@thecodeeducators.com>
 * @license  https://gnu.org/licenses/gpl.html GNU General Public License
 * @link     https://bitbucket.org/code-warrior/web-app/
 */

session_start();

if (isset($_SESSION['valid'])) {
    if ($_SESSION['valid'] !== true) {
        header("Location: ./index.php");
    }
} else {
    header("Location: ./index.php");
}

require_once "includes/db.php";
require_once "includes/main.php";

$username = select("username", "user", "username", $_SESSION['username']);

$links = getAllFileLinksFor($_SESSION['username']);

$amount_of_links = count($links);

?>
<!DOCTYPE HTML>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Web Application (v0.3.1) — Home</title>
      <link rel="stylesheet"
            href="css/normalize.css">
      <link rel="stylesheet"
            href="css/style.css">
      <link rel="stylesheet"
            href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
   </head>
   <body>
      <header>
         <h1>Home</h1>
      </header>
      <p><a href="./logout.php">Logout</a></p>
      <p>Hello, <strong><?php echo $username; ?></strong>. How’s it hanging?</p>
      <dl>
<?php
for ($index = 0; $index < $amount_of_links; $index++) {
    echo "         <dd><a href=\"uploads/$links[$index]\">$links[$index]</a></dd>\n";
}
?>
      </dl>
      <section id="upload">
         <form method="post" action="upload.php" enctype="multipart/form-data">
            <p><input type="file" name="document"></p>
            <p><input type="submit" id="send_file" value="Send File"></p>
         </form>
      </section>
   </body>
</html>
